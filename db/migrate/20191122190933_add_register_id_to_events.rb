class AddRegisterIdToEvents < ActiveRecord::Migration[6.0]
  def change
    add_column :events, :register_id, :integer
    add_index :events, :register_id
  end
end
