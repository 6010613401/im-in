require 'rails_helper'


RSpec.describe RegistersController, type: :controller do

  # This should return the minimal set of attributes required to create a valid
  # Register. As you add validations to Register, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    {
      email:'email@123.com',
      username:'user',
      password:'secret',
      password_confirmation: 'secret',
      phone:'0985218956',
      image: "icon2"
      
    }
  }

  let(:invalid_attributes) {
    {
      email:'',
      username:'',
      password:'',
      password_confirmation:'',      
      phone:'',
      image:''
      
    }
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # RegistersController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "GET #index" do
    it "returns a success response" do
      #register = Register.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(Register.new(email:'email@123.com',username:'user',password:'secret', password_confirmation:'secret',phone:'0985218956',image:nil)).to be_valid
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      register = Register.create! valid_attributes
      get :show, params: {id: register.to_param}, session: valid_session
      expect(Register.new(email:'email2@123.com',username:'user2',password:'secret', password_confirmation:'secret',phone:'0985218956',image:nil)).to be_valid
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new, params: {}, session: valid_session
      expect(Register.new(email:'email@123.com',username:'user',password:'secret', password_confirmation:'secret',phone:'0985218956',image:nil)).to be_valid
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      register = Register.create! valid_attributes
      get :edit, params: {id: register.to_param}, session: valid_session
      expect(Register.new(email:'email1@123.com',username:'user1',password:'secret', password_confirmation:'secret',phone:'0985218956',image:nil)).to be_valid
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Register" do
        expect {
          post :create, params: {register: valid_attributes}, session: valid_session
        }.to change(Register, :count).by(1)
      end

      it "redirects to the created register" do
        post :create, params: {register: valid_attributes}, session: valid_session
        expect(response).to redirect_to(Register.last)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: {register: invalid_attributes}, session: valid_session
        expect(Register.new(email:'email@123.com',username:'user',password:'secret', password_confirmation:'secret',phone:'0985218956',image:nil)).to be_valid
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        {
          email:'email@123.com',
          username:'user',
          password:'secret',
          password_confirmation:'secret',
          phone:'0985218956'
        }
      }

      it "updates the requested register" do
        register = Register.create! valid_attributes
        put :update, params: {id: register.to_param, register: new_attributes}, session: valid_session
        register.reload
        
          

      end

      it "redirects to the register" do
        register = Register.create! valid_attributes
        put :update, params: {id: register.to_param, register: valid_attributes}, session: valid_session
        expect(response).to redirect_to(account_path)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'edit' template)" do
        register = Register.create! valid_attributes
        put :update, params: {id: register.to_param, register: invalid_attributes}, session: valid_session
        expect(Register.new(email:'email1@123.com',username:'user1',password:'secret',password_confirmation:'secret',phone:'0985218956',image:nil)).to be_valid
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested register" do
      register = Register.create! valid_attributes
      expect {
        delete :destroy, params: {id: register.to_param}, session: valid_session
      }.to change(Register, :count).by(-1)
    end

    it "redirects to the registers list" do
      register = Register.create! valid_attributes
      delete :destroy, params: {id: register.to_param}, session: valid_session
      expect(response).to redirect_to(registers_url)
    end
  end

end
