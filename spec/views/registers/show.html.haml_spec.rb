require 'rails_helper'

RSpec.describe "registers/show", type: :view do
  before(:each) do
    @register = assign(:register, Register.create!(
      :email => "email@123.com",
      :username => "user",
      :password => "secret",
      :password_confirmation => "secret",
      :phone => "0985218956"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect("email@123.com").to match("email@123.com")
    expect(:username).to match("user")
    expect(:password).to match(//)
    expect(:password_confirmation).to match(//)
    expect("0985218956").to match("0985218956")
  end
end
