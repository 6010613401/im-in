  Given /^the following events:$/ do |events|
    Event.create!(events.hashes)
  end
  
  When /^I fill in "([^"]*)"  with "([^"])$/ do |value, field_id|
    fill_in field_id, :with => value
  end
  
  And /^I fill in "([^"]*)"  with "([^"])$/ do |value, field_id|
    fill_in field_id, :with => value
  end
  
  Then /^I press in "([^"]*)"  button$/ do |button|
    click_button "Save"
  end
  
  When /^I delete the (\d+)(?:st|nd|rd|th|||||) event$/ do |pos|
    visit events_path
    within("table tr:nth-child(#{pos.to_i})") do
      click_link "Destroy"
    end
  end

  Then /^I should see the following events:$/ do |expected_events_table|
    rows = find("table").all('tr')
    table = rows.map{ |r| r.all('th,td').map{|c| c.text.strip}}
    expected_events_table.diff!(table)
  end
  
  
  
