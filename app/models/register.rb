class Register < ApplicationRecord
    has_secure_password
    validates :email, uniqueness: true,format: { with: /\A[^@\s]+@([^@.\s]+\.)+[^@.\s]+\z/ }
    validates :password, presence: true
    validates :password_confirmation, presence: true
    validates :username, uniqueness: true
    validates :phone, presence: true , format: { with: /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/ } 
    mount_uploader :image, ImageUploader 
    has_many :events
end
