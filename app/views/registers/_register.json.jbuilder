json.extract! register, :id, :email, :username, :phone,:image, :created_at, :updated_at
json.url register_url(register, format: :json)
